#
#  Copyright (c) 2018 - Present  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt


where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile

EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP:=urlApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_INCLUDES += -I/usr/include/libxml2
USR_INCLUDES += -I$(E3_SITEMODS_PATH)/adgraphicsmagick/$(adgraphicsmagick_VERSION)/include/Magick++/lib
LIB_SYS_LIBS += xml2
LIB_SYS_LIBS += jpeg

SOURCES += $(APPSRC)/URLDriver.cpp

DBDS += $(APPSRC)/URLDriverSupport.dbd

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
USR_DBFLAGS += -I $(E3_SITEMODS_PATH)/adcore/$(adcore_VERSION)/db

TMPS=$(wildcard $(APPDB)/*.template)

vlibs:

.PHONY: vlibs
